#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Pull latest
git -C "$DIR" pull

gradle build -b "$DIR/build.gradle" -x test

mvn install:install-file -Dfile=$DIR/build/libs/OWLApi-0.1.jar -DgroupId=nl.wur.ssb.OWLApi -DartifactId=OWLApi -Dversion=0.1 -Dpackaging=jar