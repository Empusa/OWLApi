package org.w3.rdfschema.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://www.w3.org/2000/01/rdf-schema# ontology
 */
public interface Expression extends OWLThing {
}
