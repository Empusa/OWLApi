package org.w3.rdfschema.domain;

import java.util.List;

/**
 * Code generated from http://www.w3.org/2000/01/rdf-schema# ontology
 */
public interface Datatype extends Expression {
  Datatype getIntersectionOf(int index);

  List<? extends Datatype> getAllIntersectionOf();

  void addIntersectionOf(Datatype val);

  void setIntersectionOf(Datatype val, int index);

  void remIntersectionOf(Datatype val);

  Datatype getUnionOf(int index);

  List<? extends Datatype> getAllUnionOf();

  void addUnionOf(Datatype val);

  void setUnionOf(Datatype val, int index);

  void remUnionOf(Datatype val);

  Datatype getDatatypeComplementOf();

  void setDatatypeComplementOf(Datatype val);

  Datatype getOneOf(int index);

  List<? extends Datatype> getAllOneOf();

  void addOneOf(Datatype val);

  void setOneOf(Datatype val, int index);

  void remOneOf(Datatype val);
}
