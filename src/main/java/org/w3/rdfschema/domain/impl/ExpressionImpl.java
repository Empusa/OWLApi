package org.w3.rdfschema.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.rdfschema.domain.Expression;

/**
 * Code generated from http://www.w3.org/2000/01/rdf-schema# ontology
 */
public class ExpressionImpl extends OWLThingImpl implements Expression {
  public static final String TypeIRI = "http://www.w3.org/2000/01/rdf-schema#Expression";

  protected ExpressionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Expression make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ExpressionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Expression.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Expression.class,false);
          if(toRet == null) {
            toRet = new ExpressionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Expression)) {
          throw new RuntimeException("Instance of org.w3.rdfschema.domain.impl.ExpressionImpl expected");
        }
      }
      return (Expression)toRet;
    }
  }

  public void validate() {
  }
}
