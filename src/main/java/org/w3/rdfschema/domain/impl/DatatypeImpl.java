package org.w3.rdfschema.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.rdfschema.domain.Datatype;

/**
 * Code generated from http://www.w3.org/2000/01/rdf-schema# ontology
 */
public class DatatypeImpl extends ExpressionImpl implements Datatype {
  public static final String TypeIRI = "http://www.w3.org/2000/01/rdf-schema#Datatype";

  protected DatatypeImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Datatype make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new DatatypeImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Datatype.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Datatype.class,false);
          if(toRet == null) {
            toRet = new DatatypeImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Datatype)) {
          throw new RuntimeException("Instance of org.w3.rdfschema.domain.impl.DatatypeImpl expected");
        }
      }
      return (Datatype)toRet;
    }
  }

  public void validate() {
  }

  public Datatype getIntersectionOf(int index) {
    return this.getRefListAtIndexList("http://www.w3.org/2002/07/owl#intersectionOf",true,Datatype.class,index);
  }

  public List<? extends Datatype> getAllIntersectionOf() {
    return this.getRefListList("http://www.w3.org/2002/07/owl#intersectionOf",true,Datatype.class);
  }

  public void addIntersectionOf(Datatype val) {
    this.addRefListList("http://www.w3.org/2002/07/owl#intersectionOf",val);
  }

  public void setIntersectionOf(Datatype val, int index) {
    this.setRefListList("http://www.w3.org/2002/07/owl#intersectionOf",val,true,index);
  }

  public void remIntersectionOf(Datatype val) {
    this.remRefListList("http://www.w3.org/2002/07/owl#intersectionOf",val,true);
  }

  public Datatype getUnionOf(int index) {
    return this.getRefListAtIndexList("http://www.w3.org/2002/07/owl#unionOf",true,Datatype.class,index);
  }

  public List<? extends Datatype> getAllUnionOf() {
    return this.getRefListList("http://www.w3.org/2002/07/owl#unionOf",true,Datatype.class);
  }

  public void addUnionOf(Datatype val) {
    this.addRefListList("http://www.w3.org/2002/07/owl#unionOf",val);
  }

  public void setUnionOf(Datatype val, int index) {
    this.setRefListList("http://www.w3.org/2002/07/owl#unionOf",val,true,index);
  }

  public void remUnionOf(Datatype val) {
    this.remRefListList("http://www.w3.org/2002/07/owl#unionOf",val,true);
  }

  public Datatype getDatatypeComplementOf() {
    return this.getRef("http://www.w3.org/2002/07/owl#datatypeComplementOf",true,Datatype.class);
  }

  public void setDatatypeComplementOf(Datatype val) {
    this.setRef("http://www.w3.org/2002/07/owl#datatypeComplementOf",val,Datatype.class);
  }

  public Datatype getOneOf(int index) {
    return this.getRefListAtIndexList("http://www.w3.org/2002/07/owl#oneOf",true,Datatype.class,index);
  }

  public List<? extends Datatype> getAllOneOf() {
    return this.getRefListList("http://www.w3.org/2002/07/owl#oneOf",true,Datatype.class);
  }

  public void addOneOf(Datatype val) {
    this.addRefListList("http://www.w3.org/2002/07/owl#oneOf",val);
  }

  public void setOneOf(Datatype val, int index) {
    this.setRefListList("http://www.w3.org/2002/07/owl#oneOf",val,true,index);
  }

  public void remOneOf(Datatype val) {
    this.remRefListList("http://www.w3.org/2002/07/owl#oneOf",val,true);
  }
}
