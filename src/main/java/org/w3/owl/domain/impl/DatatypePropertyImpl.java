package org.w3.owl.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.owl.domain.ClassExpression;
import org.w3.owl.domain.DatatypeProperty;
import org.w3.rdfschema.domain.Datatype;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public class DatatypePropertyImpl extends PropertyImpl implements DatatypeProperty {
  public static final String TypeIRI = "http://www.w3.org/2002/07/owl#DatatypeProperty";

  protected DatatypePropertyImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static DatatypeProperty make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new DatatypePropertyImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,DatatypeProperty.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,DatatypeProperty.class,false);
          if(toRet == null) {
            toRet = new DatatypePropertyImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof DatatypeProperty)) {
          throw new RuntimeException("Instance of org.w3.owl.domain.impl.DatatypePropertyImpl expected");
        }
      }
      return (DatatypeProperty)toRet;
    }
  }

  public void validate() {
  }

  public void remSubPropertyOf(DatatypeProperty val) {
    this.remRef("http://www.w3.org/2000/01/rdf-schema#subPropertyOf",val,true);
  }

  public List<? extends DatatypeProperty> getAllSubPropertyOf() {
    return this.getRefSet("http://www.w3.org/2000/01/rdf-schema#subPropertyOf",true,DatatypeProperty.class);
  }

  public void addSubPropertyOf(DatatypeProperty val) {
    this.addRef("http://www.w3.org/2000/01/rdf-schema#subPropertyOf",val);
  }

  public DatatypeProperty getEquivalentProperty() {
    return this.getRef("http://www.w3.org/2002/07/owl#equivalentProperty",true,DatatypeProperty.class);
  }

  public void setEquivalentProperty(DatatypeProperty val) {
    this.setRef("http://www.w3.org/2002/07/owl#equivalentProperty",val,DatatypeProperty.class);
  }

  public DatatypeProperty getPropertyDisjointWith() {
    return this.getRef("http://www.w3.org/2002/07/owl#propertyDisjointWith",true,DatatypeProperty.class);
  }

  public void setPropertyDisjointWith(DatatypeProperty val) {
    this.setRef("http://www.w3.org/2002/07/owl#propertyDisjointWith",val,DatatypeProperty.class);
  }

  public DatatypeProperty getInverseOf() {
    return this.getRef("http://www.w3.org/2002/07/owl#inverseOf",true,DatatypeProperty.class);
  }

  public void setInverseOf(DatatypeProperty val) {
    this.setRef("http://www.w3.org/2002/07/owl#inverseOf",val,DatatypeProperty.class);
  }

  public Datatype getRange() {
    return this.getRef("http://www.w3.org/2000/01/rdf-schema#range",true,Datatype.class);
  }

  public void setRange(Datatype val) {
    this.setRef("http://www.w3.org/2000/01/rdf-schema#range",val,Datatype.class);
  }

  public String getDefinition() {
    return this.getStringLit("http://www.w3.org/2004/02/skos/core#definition",true);
  }

  public void setDefinition(String val) {
    this.setStringLit("http://www.w3.org/2004/02/skos/core#definition",val);
  }

  public ClassExpression getDomain() {
    return this.getRef("http://www.w3.org/2000/01/rdf-schema#domain",true,ClassExpression.class);
  }

  public void setDomain(ClassExpression val) {
    this.setRef("http://www.w3.org/2000/01/rdf-schema#domain",val,ClassExpression.class);
  }
}
