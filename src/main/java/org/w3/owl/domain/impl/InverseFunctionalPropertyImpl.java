package org.w3.owl.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.owl.domain.ClassExpression;
import org.w3.owl.domain.InverseFunctionalProperty;
import org.w3.owl.domain.ObjectProperty;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public class InverseFunctionalPropertyImpl extends ObjectPropertyImpl implements InverseFunctionalProperty {
  public static final String TypeIRI = "http://www.w3.org/2002/07/owl#InverseFunctionalProperty";

  protected InverseFunctionalPropertyImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static InverseFunctionalProperty make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new InverseFunctionalPropertyImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,InverseFunctionalProperty.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,InverseFunctionalProperty.class,false);
          if(toRet == null) {
            toRet = new InverseFunctionalPropertyImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof InverseFunctionalProperty)) {
          throw new RuntimeException("Instance of org.w3.owl.domain.impl.InverseFunctionalPropertyImpl expected");
        }
      }
      return (InverseFunctionalProperty)toRet;
    }
  }

  public void validate() {
  }

  public void remSubPropertyOf(ObjectProperty val) {
    this.remRef("http://www.w3.org/2000/01/rdf-schema#subPropertyOf",val,true);
  }

  public List<? extends ObjectProperty> getAllSubPropertyOf() {
    return this.getRefSet("http://www.w3.org/2000/01/rdf-schema#subPropertyOf",true,ObjectProperty.class);
  }

  public void addSubPropertyOf(ObjectProperty val) {
    this.addRef("http://www.w3.org/2000/01/rdf-schema#subPropertyOf",val);
  }

  public ObjectProperty getEquivalentProperty() {
    return this.getRef("http://www.w3.org/2002/07/owl#equivalentProperty",true,ObjectProperty.class);
  }

  public void setEquivalentProperty(ObjectProperty val) {
    this.setRef("http://www.w3.org/2002/07/owl#equivalentProperty",val,ObjectProperty.class);
  }

  public ObjectProperty getPropertyDisjointWith() {
    return this.getRef("http://www.w3.org/2002/07/owl#propertyDisjointWith",true,ObjectProperty.class);
  }

  public void setPropertyDisjointWith(ObjectProperty val) {
    this.setRef("http://www.w3.org/2002/07/owl#propertyDisjointWith",val,ObjectProperty.class);
  }

  public ObjectProperty getInverseOf() {
    return this.getRef("http://www.w3.org/2002/07/owl#inverseOf",true,ObjectProperty.class);
  }

  public void setInverseOf(ObjectProperty val) {
    this.setRef("http://www.w3.org/2002/07/owl#inverseOf",val,ObjectProperty.class);
  }

  public ClassExpression getRange() {
    return this.getRef("http://www.w3.org/2000/01/rdf-schema#range",true,ClassExpression.class);
  }

  public void setRange(ClassExpression val) {
    this.setRef("http://www.w3.org/2000/01/rdf-schema#range",val,ClassExpression.class);
  }

  public String getDefinition() {
    return this.getStringLit("http://www.w3.org/2004/02/skos/core#definition",true);
  }

  public void setDefinition(String val) {
    this.setStringLit("http://www.w3.org/2004/02/skos/core#definition",val);
  }

  public ClassExpression getDomain() {
    return this.getRef("http://www.w3.org/2000/01/rdf-schema#domain",true,ClassExpression.class);
  }

  public void setDomain(ClassExpression val) {
    this.setRef("http://www.w3.org/2000/01/rdf-schema#domain",val,ClassExpression.class);
  }
}
