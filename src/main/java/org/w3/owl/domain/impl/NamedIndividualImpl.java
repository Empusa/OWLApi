package org.w3.owl.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.owl.domain.Class;
import org.w3.owl.domain.NamedIndividual;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public class NamedIndividualImpl extends OWLThingImpl implements NamedIndividual {
  public static final String TypeIRI = "http://www.w3.org/2002/07/owl#NamedIndividual";

  protected NamedIndividualImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static NamedIndividual make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new NamedIndividualImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,NamedIndividual.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,NamedIndividual.class,false);
          if(toRet == null) {
            toRet = new NamedIndividualImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof NamedIndividual)) {
          throw new RuntimeException("Instance of org.w3.owl.domain.impl.NamedIndividualImpl expected");
        }
      }
      return (NamedIndividual)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
  }

  public void remSameAs(NamedIndividual val) {
    this.remRef("http://www.w3.org/2002/07/owl#sameAs",val,true);
  }

  public List<? extends NamedIndividual> getAllSameAs() {
    return this.getRefSet("http://www.w3.org/2002/07/owl#sameAs",true,NamedIndividual.class);
  }

  public void addSameAs(NamedIndividual val) {
    this.addRef("http://www.w3.org/2002/07/owl#sameAs",val);
  }

  public void remDifferentFrom(NamedIndividual val) {
    this.remRef("http://www.w3.org/2002/07/owl#differentFrom",val,true);
  }

  public List<? extends NamedIndividual> getAllDifferentFrom() {
    return this.getRefSet("http://www.w3.org/2002/07/owl#differentFrom",true,NamedIndividual.class);
  }

  public void addDifferentFrom(NamedIndividual val) {
    this.addRef("http://www.w3.org/2002/07/owl#differentFrom",val);
  }

  public Class getType() {
    return this.getRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type",false,Class.class);
  }

  public void setType(Class val) {
    this.setRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type",val,Class.class);
  }
}
