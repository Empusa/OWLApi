package org.w3.owl.domain.impl;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.owl.domain.ClassExpression;
import org.w3.owl.domain.NamedIndividual;
import org.w3.owl.domain.Property;
import org.w3.owl.domain.Restriction;
import org.w3.rdfschema.domain.Datatype;
import org.w3.rdfschema.domain.Expression;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public class RestrictionImpl extends ClassExpressionImpl implements Restriction {
  public static final String TypeIRI = "http://www.w3.org/2002/07/owl#Restriction";

  protected RestrictionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Restriction make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new RestrictionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Restriction.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Restriction.class,false);
          if(toRet == null) {
            toRet = new RestrictionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Restriction)) {
          throw new RuntimeException("Instance of org.w3.owl.domain.impl.RestrictionImpl expected");
        }
      }
      return (Restriction)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://www.w3.org/2002/07/owl#onProperty");
  }

  public Integer getMinQualifiedCardinality() {
    return this.getIntegerLit("http://www.w3.org/2002/07/owl#minQualifiedCardinality",true);
  }

  public void setMinQualifiedCardinality(Integer val) {
    this.setIntegerLit("http://www.w3.org/2002/07/owl#minQualifiedCardinality",val);
  }

  public Property getOnProperty() {
    return this.getRef("http://www.w3.org/2002/07/owl#onProperty",false,Property.class);
  }

  public void setOnProperty(Property val) {
    this.setRef("http://www.w3.org/2002/07/owl#onProperty",val,Property.class);
  }

  public ClassExpression getOnClass() {
    return this.getRef("http://www.w3.org/2002/07/owl#onClass",true,ClassExpression.class);
  }

  public void setOnClass(ClassExpression val) {
    this.setRef("http://www.w3.org/2002/07/owl#onClass",val,ClassExpression.class);
  }

  public Expression getAllValuesFrom() {
    return this.getRef("http://www.w3.org/2002/07/owl#allValuesFrom",true,Expression.class);
  }

  public void setAllValuesFrom(Expression val) {
    this.setRef("http://www.w3.org/2002/07/owl#allValuesFrom",val,Expression.class);
  }

  public Datatype getOnDataRange() {
    return this.getRef("http://www.w3.org/2002/07/owl#onDataRange",true,Datatype.class);
  }

  public void setOnDataRange(Datatype val) {
    this.setRef("http://www.w3.org/2002/07/owl#onDataRange",val,Datatype.class);
  }

  public Integer getMaxQualifiedCardinality() {
    return this.getIntegerLit("http://www.w3.org/2002/07/owl#maxQualifiedCardinality",true);
  }

  public void setMaxQualifiedCardinality(Integer val) {
    this.setIntegerLit("http://www.w3.org/2002/07/owl#maxQualifiedCardinality",val);
  }

  public Integer getQualifiedCardinality() {
    return this.getIntegerLit("http://www.w3.org/2002/07/owl#qualifiedCardinality",true);
  }

  public void setQualifiedCardinality(Integer val) {
    this.setIntegerLit("http://www.w3.org/2002/07/owl#qualifiedCardinality",val);
  }

  public Expression getSomeValuesFrom() {
    return this.getRef("http://www.w3.org/2002/07/owl#someValuesFrom",true,Expression.class);
  }

  public void setSomeValuesFrom(Expression val) {
    this.setRef("http://www.w3.org/2002/07/owl#someValuesFrom",val,Expression.class);
  }

  public Integer getCardinality() {
    return this.getIntegerLit("http://www.w3.org/2002/07/owl#cardinality",true);
  }

  public void setCardinality(Integer val) {
    this.setIntegerLit("http://www.w3.org/2002/07/owl#cardinality",val);
  }

  public NamedIndividual getHasValue() {
    return this.getRef("http://www.w3.org/2002/07/owl#hasValue",true,NamedIndividual.class);
  }

  public void setHasValue(NamedIndividual val) {
    this.setRef("http://www.w3.org/2002/07/owl#hasValue",val,NamedIndividual.class);
  }

  public Boolean getHasSelf() {
    return this.getBooleanLit("http://www.w3.org/2002/07/owl#hasSelf",true);
  }

  public void setHasSelf(Boolean val) {
    this.setBooleanLit("http://www.w3.org/2002/07/owl#hasSelf",val);
  }

  public Integer getMaxCardinality() {
    return this.getIntegerLit("http://www.w3.org/2002/07/owl#maxCardinality",true);
  }

  public void setMaxCardinality(Integer val) {
    this.setIntegerLit("http://www.w3.org/2002/07/owl#maxCardinality",val);
  }

  public Integer getMinCardinality() {
    return this.getIntegerLit("http://www.w3.org/2002/07/owl#minCardinality",true);
  }

  public void setMinCardinality(Integer val) {
    this.setIntegerLit("http://www.w3.org/2002/07/owl#minCardinality",val);
  }

  public void remSubClassOf(ClassExpression val) {
    this.remRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",val,true);
  }

  public List<? extends ClassExpression> getAllSubClassOf() {
    return this.getRefSet("http://www.w3.org/2000/01/rdf-schema#subClassOf",true,ClassExpression.class);
  }

  public void addSubClassOf(ClassExpression val) {
    this.addRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",val);
  }

  public void remDisjointWith(ClassExpression val) {
    this.remRef("http://www.w3.org/2002/07/owl#disjointWith",val,true);
  }

  public List<? extends ClassExpression> getAllDisjointWith() {
    return this.getRefSet("http://www.w3.org/2002/07/owl#disjointWith",true,ClassExpression.class);
  }

  public void addDisjointWith(ClassExpression val) {
    this.addRef("http://www.w3.org/2002/07/owl#disjointWith",val);
  }

  public ClassExpression getEquivalentClass() {
    return this.getRef("http://www.w3.org/2002/07/owl#equivalentClass",true,ClassExpression.class);
  }

  public void setEquivalentClass(ClassExpression val) {
    this.setRef("http://www.w3.org/2002/07/owl#equivalentClass",val,ClassExpression.class);
  }
}
