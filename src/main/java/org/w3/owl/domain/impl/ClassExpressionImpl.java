package org.w3.owl.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.owl.domain.ClassExpression;
import org.w3.rdfschema.domain.impl.ExpressionImpl;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public class ClassExpressionImpl extends ExpressionImpl implements ClassExpression {
  public static final String TypeIRI = "http://www.w3.org/2002/07/owl#ClassExpression";

  protected ClassExpressionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ClassExpression make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ClassExpressionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ClassExpression.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ClassExpression.class,false);
          if(toRet == null) {
            toRet = new ClassExpressionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ClassExpression)) {
          throw new RuntimeException("Instance of org.w3.owl.domain.impl.ClassExpressionImpl expected");
        }
      }
      return (ClassExpression)toRet;
    }
  }

  public void validate() {
  }

  public void remSubClassOf(ClassExpression val) {
    this.remRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",val,true);
  }

  public List<? extends ClassExpression> getAllSubClassOf() {
    return this.getRefSet("http://www.w3.org/2000/01/rdf-schema#subClassOf",true,ClassExpression.class);
  }

  public void addSubClassOf(ClassExpression val) {
    this.addRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",val);
  }

  public void remDisjointWith(ClassExpression val) {
    this.remRef("http://www.w3.org/2002/07/owl#disjointWith",val,true);
  }

  public List<? extends ClassExpression> getAllDisjointWith() {
    return this.getRefSet("http://www.w3.org/2002/07/owl#disjointWith",true,ClassExpression.class);
  }

  public void addDisjointWith(ClassExpression val) {
    this.addRef("http://www.w3.org/2002/07/owl#disjointWith",val);
  }

  public ClassExpression getEquivalentClass() {
    return this.getRef("http://www.w3.org/2002/07/owl#equivalentClass",true,ClassExpression.class);
  }

  public void setEquivalentClass(ClassExpression val) {
    this.setRef("http://www.w3.org/2002/07/owl#equivalentClass",val,ClassExpression.class);
  }
}
