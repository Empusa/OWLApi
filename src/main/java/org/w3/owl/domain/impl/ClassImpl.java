package org.w3.owl.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.owl.domain.Class;
import org.w3.owl.domain.ClassExpression;
import org.w3.owl.domain.NamedIndividual;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public class ClassImpl extends ClassExpressionImpl implements Class {
  public static final String TypeIRI = "http://www.w3.org/2002/07/owl#Class";

  protected ClassImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Class make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ClassImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Class.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Class.class,false);
          if(toRet == null) {
            toRet = new ClassImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Class)) {
          throw new RuntimeException("Instance of org.w3.owl.domain.impl.ClassImpl expected");
        }
      }
      return (Class)toRet;
    }
  }

  public void validate() {
  }

  public ClassExpression getComplementOf() {
    return this.getRef("http://www.w3.org/2002/07/owl#complementOf",true,ClassExpression.class);
  }

  public void setComplementOf(ClassExpression val) {
    this.setRef("http://www.w3.org/2002/07/owl#complementOf",val,ClassExpression.class);
  }

  public String getDefinition() {
    return this.getStringLit("http://www.w3.org/2004/02/skos/core#definition",true);
  }

  public void setDefinition(String val) {
    this.setStringLit("http://www.w3.org/2004/02/skos/core#definition",val);
  }

  public ClassExpression getIntersectionOf(int index) {
    return this.getRefListAtIndexList("http://www.w3.org/2002/07/owl#intersectionOf",true,ClassExpression.class,index);
  }

  public List<? extends ClassExpression> getAllIntersectionOf() {
    return this.getRefListList("http://www.w3.org/2002/07/owl#intersectionOf",true,ClassExpression.class);
  }

  public void addIntersectionOf(ClassExpression val) {
    this.addRefListList("http://www.w3.org/2002/07/owl#intersectionOf",val);
  }

  public void setIntersectionOf(ClassExpression val, int index) {
    this.setRefListList("http://www.w3.org/2002/07/owl#intersectionOf",val,true,index);
  }

  public void remIntersectionOf(ClassExpression val) {
    this.remRefListList("http://www.w3.org/2002/07/owl#intersectionOf",val,true);
  }

  public ClassExpression getUnionOf(int index) {
    return this.getRefListAtIndexList("http://www.w3.org/2002/07/owl#unionOf",true,ClassExpression.class,index);
  }

  public List<? extends ClassExpression> getAllUnionOf() {
    return this.getRefListList("http://www.w3.org/2002/07/owl#unionOf",true,ClassExpression.class);
  }

  public void addUnionOf(ClassExpression val) {
    this.addRefListList("http://www.w3.org/2002/07/owl#unionOf",val);
  }

  public void setUnionOf(ClassExpression val, int index) {
    this.setRefListList("http://www.w3.org/2002/07/owl#unionOf",val,true,index);
  }

  public void remUnionOf(ClassExpression val) {
    this.remRefListList("http://www.w3.org/2002/07/owl#unionOf",val,true);
  }

  public ClassExpression getDisjointUnionOf(int index) {
    return this.getRefListAtIndexList("http://www.w3.org/2002/07/owl#disjointUnionOf",true,ClassExpression.class,index);
  }

  public List<? extends ClassExpression> getAllDisjointUnionOf() {
    return this.getRefListList("http://www.w3.org/2002/07/owl#disjointUnionOf",true,ClassExpression.class);
  }

  public void addDisjointUnionOf(ClassExpression val) {
    this.addRefListList("http://www.w3.org/2002/07/owl#disjointUnionOf",val);
  }

  public void setDisjointUnionOf(ClassExpression val, int index) {
    this.setRefListList("http://www.w3.org/2002/07/owl#disjointUnionOf",val,true,index);
  }

  public void remDisjointUnionOf(ClassExpression val) {
    this.remRefListList("http://www.w3.org/2002/07/owl#disjointUnionOf",val,true);
  }

  public String getPropertyDefinitions() {
    return this.getStringLit("http://empusa.org/0.1/propertyDefinitions",true);
  }

  public void setPropertyDefinitions(String val) {
    this.setStringLit("http://empusa.org/0.1/propertyDefinitions",val);
  }

  public NamedIndividual getOneOf(int index) {
    return this.getRefListAtIndexList("http://www.w3.org/2002/07/owl#oneOf",true,NamedIndividual.class,index);
  }

  public List<? extends NamedIndividual> getAllOneOf() {
    return this.getRefListList("http://www.w3.org/2002/07/owl#oneOf",true,NamedIndividual.class);
  }

  public void addOneOf(NamedIndividual val) {
    this.addRefListList("http://www.w3.org/2002/07/owl#oneOf",val);
  }

  public void setOneOf(NamedIndividual val, int index) {
    this.setRefListList("http://www.w3.org/2002/07/owl#oneOf",val,true,index);
  }

  public void remOneOf(NamedIndividual val) {
    this.remRefListList("http://www.w3.org/2002/07/owl#oneOf",val,true);
  }

  public void remSubClassOf(ClassExpression val) {
    this.remRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",val,true);
  }

  public List<? extends ClassExpression> getAllSubClassOf() {
    return this.getRefSet("http://www.w3.org/2000/01/rdf-schema#subClassOf",true,ClassExpression.class);
  }

  public void addSubClassOf(ClassExpression val) {
    this.addRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",val);
  }

  public void remDisjointWith(ClassExpression val) {
    this.remRef("http://www.w3.org/2002/07/owl#disjointWith",val,true);
  }

  public List<? extends ClassExpression> getAllDisjointWith() {
    return this.getRefSet("http://www.w3.org/2002/07/owl#disjointWith",true,ClassExpression.class);
  }

  public void addDisjointWith(ClassExpression val) {
    this.addRef("http://www.w3.org/2002/07/owl#disjointWith",val);
  }

  public ClassExpression getEquivalentClass() {
    return this.getRef("http://www.w3.org/2002/07/owl#equivalentClass",true,ClassExpression.class);
  }

  public void setEquivalentClass(ClassExpression val) {
    this.setRef("http://www.w3.org/2002/07/owl#equivalentClass",val,ClassExpression.class);
  }
}
