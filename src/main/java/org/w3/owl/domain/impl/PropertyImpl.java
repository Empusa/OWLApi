package org.w3.owl.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.owl.domain.ClassExpression;
import org.w3.owl.domain.Property;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public class PropertyImpl extends OWLThingImpl implements Property {
  public static final String TypeIRI = "http://www.w3.org/2002/07/owl#Property";

  protected PropertyImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Property make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PropertyImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Property.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Property.class,false);
          if(toRet == null) {
            toRet = new PropertyImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Property)) {
          throw new RuntimeException("Instance of org.w3.owl.domain.impl.PropertyImpl expected");
        }
      }
      return (Property)toRet;
    }
  }

  public void validate() {
  }

  public String getDefinition() {
    return this.getStringLit("http://www.w3.org/2004/02/skos/core#definition",true);
  }

  public void setDefinition(String val) {
    this.setStringLit("http://www.w3.org/2004/02/skos/core#definition",val);
  }

  public ClassExpression getDomain() {
    return this.getRef("http://www.w3.org/2000/01/rdf-schema#domain",true,ClassExpression.class);
  }

  public void setDomain(ClassExpression val) {
    this.setRef("http://www.w3.org/2000/01/rdf-schema#domain",val,ClassExpression.class);
  }
}
