package org.w3.owl.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.owl.domain.Ontology;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public class OntologyImpl extends OWLThingImpl implements Ontology {
  public static final String TypeIRI = "http://www.w3.org/2002/07/owl#Ontology";

  protected OntologyImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Ontology make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new OntologyImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Ontology.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Ontology.class,false);
          if(toRet == null) {
            toRet = new OntologyImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Ontology)) {
          throw new RuntimeException("Instance of org.w3.owl.domain.impl.OntologyImpl expected");
        }
      }
      return (Ontology)toRet;
    }
  }

  public void validate() {
  }
}
