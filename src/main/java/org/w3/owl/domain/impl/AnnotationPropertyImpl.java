package org.w3.owl.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.owl.domain.AnnotationProperty;
import org.w3.owl.domain.ClassExpression;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public class AnnotationPropertyImpl extends OWLThingImpl implements AnnotationProperty {
  public static final String TypeIRI = "http://www.w3.org/2002/07/owl#AnnotationProperty";

  protected AnnotationPropertyImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static AnnotationProperty make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AnnotationPropertyImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,AnnotationProperty.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,AnnotationProperty.class,false);
          if(toRet == null) {
            toRet = new AnnotationPropertyImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof AnnotationProperty)) {
          throw new RuntimeException("Instance of org.w3.owl.domain.impl.AnnotationPropertyImpl expected");
        }
      }
      return (AnnotationProperty)toRet;
    }
  }

  public void validate() {
  }

  public void remSubPropertyOf(AnnotationProperty val) {
    this.remRef("http://www.w3.org/2000/01/rdf-schema#subPropertyOf",val,true);
  }

  public List<? extends AnnotationProperty> getAllSubPropertyOf() {
    return this.getRefSet("http://www.w3.org/2000/01/rdf-schema#subPropertyOf",true,AnnotationProperty.class);
  }

  public void addSubPropertyOf(AnnotationProperty val) {
    this.addRef("http://www.w3.org/2000/01/rdf-schema#subPropertyOf",val);
  }

  public ClassExpression getDomain() {
    return this.getRef("http://www.w3.org/2000/01/rdf-schema#domain",true,ClassExpression.class);
  }

  public void setDomain(ClassExpression val) {
    this.setRef("http://www.w3.org/2000/01/rdf-schema#domain",val,ClassExpression.class);
  }

  public ClassExpression getRange() {
    return this.getRef("http://www.w3.org/2000/01/rdf-schema#range",true,ClassExpression.class);
  }

  public void setRange(ClassExpression val) {
    this.setRef("http://www.w3.org/2000/01/rdf-schema#range",val,ClassExpression.class);
  }
}
