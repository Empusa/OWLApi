package org.w3.owl.domain;

import java.util.List;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public interface ObjectProperty extends Property {
  void remSubPropertyOf(ObjectProperty val);

  List<? extends ObjectProperty> getAllSubPropertyOf();

  void addSubPropertyOf(ObjectProperty val);

  ObjectProperty getEquivalentProperty();

  void setEquivalentProperty(ObjectProperty val);

  ObjectProperty getPropertyDisjointWith();

  void setPropertyDisjointWith(ObjectProperty val);

  ObjectProperty getInverseOf();

  void setInverseOf(ObjectProperty val);

  ClassExpression getRange();

  void setRange(ClassExpression val);
}
