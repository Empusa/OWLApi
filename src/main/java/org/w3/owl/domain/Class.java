package org.w3.owl.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public interface Class extends ClassExpression {
  ClassExpression getComplementOf();

  void setComplementOf(ClassExpression val);

  String getDefinition();

  void setDefinition(String val);

  ClassExpression getIntersectionOf(int index);

  List<? extends ClassExpression> getAllIntersectionOf();

  void addIntersectionOf(ClassExpression val);

  void setIntersectionOf(ClassExpression val, int index);

  void remIntersectionOf(ClassExpression val);

  ClassExpression getUnionOf(int index);

  List<? extends ClassExpression> getAllUnionOf();

  void addUnionOf(ClassExpression val);

  void setUnionOf(ClassExpression val, int index);

  void remUnionOf(ClassExpression val);

  ClassExpression getDisjointUnionOf(int index);

  List<? extends ClassExpression> getAllDisjointUnionOf();

  void addDisjointUnionOf(ClassExpression val);

  void setDisjointUnionOf(ClassExpression val, int index);

  void remDisjointUnionOf(ClassExpression val);

  String getPropertyDefinitions();

  void setPropertyDefinitions(String val);

  NamedIndividual getOneOf(int index);

  List<? extends NamedIndividual> getAllOneOf();

  void addOneOf(NamedIndividual val);

  void setOneOf(NamedIndividual val, int index);

  void remOneOf(NamedIndividual val);
}
