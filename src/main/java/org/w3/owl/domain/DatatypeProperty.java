package org.w3.owl.domain;

import java.util.List;
import org.w3.rdfschema.domain.Datatype;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public interface DatatypeProperty extends Property {
  void remSubPropertyOf(DatatypeProperty val);

  List<? extends DatatypeProperty> getAllSubPropertyOf();

  void addSubPropertyOf(DatatypeProperty val);

  DatatypeProperty getEquivalentProperty();

  void setEquivalentProperty(DatatypeProperty val);

  DatatypeProperty getPropertyDisjointWith();

  void setPropertyDisjointWith(DatatypeProperty val);

  DatatypeProperty getInverseOf();

  void setInverseOf(DatatypeProperty val);

  Datatype getRange();

  void setRange(Datatype val);
}
