package org.w3.owl.domain;

import java.lang.Boolean;
import java.lang.Integer;
import org.w3.rdfschema.domain.Datatype;
import org.w3.rdfschema.domain.Expression;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public interface Restriction extends ClassExpression {
  Integer getMinQualifiedCardinality();

  void setMinQualifiedCardinality(Integer val);

  Property getOnProperty();

  void setOnProperty(Property val);

  ClassExpression getOnClass();

  void setOnClass(ClassExpression val);

  Expression getAllValuesFrom();

  void setAllValuesFrom(Expression val);

  Datatype getOnDataRange();

  void setOnDataRange(Datatype val);

  Integer getMaxQualifiedCardinality();

  void setMaxQualifiedCardinality(Integer val);

  Integer getQualifiedCardinality();

  void setQualifiedCardinality(Integer val);

  Expression getSomeValuesFrom();

  void setSomeValuesFrom(Expression val);

  Integer getCardinality();

  void setCardinality(Integer val);

  NamedIndividual getHasValue();

  void setHasValue(NamedIndividual val);

  Boolean getHasSelf();

  void setHasSelf(Boolean val);

  Integer getMaxCardinality();

  void setMaxCardinality(Integer val);

  Integer getMinCardinality();

  void setMinCardinality(Integer val);
}
