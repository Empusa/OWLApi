package org.w3.owl.domain;

import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public interface NamedIndividual extends OWLThing {
  void remSameAs(NamedIndividual val);

  List<? extends NamedIndividual> getAllSameAs();

  void addSameAs(NamedIndividual val);

  void remDifferentFrom(NamedIndividual val);

  List<? extends NamedIndividual> getAllDifferentFrom();

  void addDifferentFrom(NamedIndividual val);

  Class getType();

  void setType(Class val);
}
