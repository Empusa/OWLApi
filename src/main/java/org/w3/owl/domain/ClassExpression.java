package org.w3.owl.domain;

import java.util.List;
import org.w3.rdfschema.domain.Expression;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public interface ClassExpression extends Expression {
  void remSubClassOf(ClassExpression val);

  List<? extends ClassExpression> getAllSubClassOf();

  void addSubClassOf(ClassExpression val);

  void remDisjointWith(ClassExpression val);

  List<? extends ClassExpression> getAllDisjointWith();

  void addDisjointWith(ClassExpression val);

  ClassExpression getEquivalentClass();

  void setEquivalentClass(ClassExpression val);
}
