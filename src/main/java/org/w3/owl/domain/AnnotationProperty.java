package org.w3.owl.domain;

import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://www.w3.org/2002/07/owl# ontology
 */
public interface AnnotationProperty extends OWLThing {
  void remSubPropertyOf(AnnotationProperty val);

  List<? extends AnnotationProperty> getAllSubPropertyOf();

  void addSubPropertyOf(AnnotationProperty val);

  ClassExpression getDomain();

  void setDomain(ClassExpression val);

  ClassExpression getRange();

  void setRange(ClassExpression val);
}
